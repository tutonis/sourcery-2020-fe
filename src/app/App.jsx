import React from 'react';
import './App.css';
import Login from '../features/login';

const App = () => (
  <div>
    <Login />
  </div>
);

export default App;
