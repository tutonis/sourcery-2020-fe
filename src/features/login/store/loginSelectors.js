export const getCurrentUser = state => state.login.currentUser;

export const isUserAuthenticating = state => state.login.isUserAuthenticating;
